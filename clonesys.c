int
clone(int (*fcn)(void *), void *stack, int flags, void *arg)
{

   cprintf("you are in clone");
   struct proc *thread_process;
   int pid,i;
   uint stack_top;
   
   thread_process = allocproc();
   if (thread_process == 0){
   	cprintf("No space left to create new process");
   	return -1;
   }
   
   thread_process->parent = myproc(); 
   thread_process->pgdir = myproc()->pgdir; 
   thread_process->sz = myproc()->sz; 
   thread_process->tf = myproc()->tf; 
   
   thread_process->kstack = kalloc();
   if(thread_process->kstack == 0)
   {
   	cprintf("Kernel stack not allocated");
   	thread_process->kstack = UNUSED;
   	return -1;
   }
   
   //copyuvm(thread_process->parent->pgdir, thread_process->parent->sz);
   
   if (copyuvm(myproc()->pgdir, myproc()->sz) < 0) {
      cprintf("Failed to copy address space to new thread");
      //deallocuvm(thread_process->pgdir, USERTOP - PGSIZE, USERTOP);
      kfree(thread_process->kstack);
      thread_process->kstack = UNUSED;
      return -1;
   }
   thread_process->tf = (struct trapframe *)(thread_process->kstack + KSTACKSIZE) - 1;

  // Set the new thread's instruction pointer to the start function
  thread_process->tf->eip = (uint)fcn;

  // Set the new thread's argument pointer to the argument
  thread_process->tf->esp = (uint)stack + PGSIZE - 4;
  *(uint *)(thread_process->tf->esp) = (uint)arg;

  // Set the new thread's user stack pointer to the top of the stack
  stack_top = (uint)stack + PGSIZE;
  thread_process->tf->esp -= 4;
  *(uint *)(thread_process->tf->esp) = stack_top;
   /*
   thread_process->tf->eax = 0;
   thread_process->tf->esp = (uint)stack + PGSIZE;
   thread_process->tf->eip = (uint)fn;
   
   thread_process->tf->esp = (uint)stack + PGSIZE - 4; // align stack pointer
   *(uint*)(thread_process->tf->esp) = (uint)arg; // push arg onto stack
   thread_process->tf->esp -= 4; // make space for fake return address
   *(uint*)(thread_process->tf->esp) = 0xffffffff; // fake return address
   thread_process->tf->eip = (uint)fn;*/
   
   //*(void**)(stack + PGSIZE - sizeof(arg)) = arg;
   
   for(i = 0; i < NOFILE; i++){
    if(myproc()->ofile[i])
      thread_process->ofile[i] = filedup(myproc()->ofile[i]);
   }
   thread_process->cwd = idup(myproc()->cwd);

   thread_process->state = RUNNABLE;
   pid = thread_process->pid;
   
   cprintf("\nend\n");
   return pid;
}
